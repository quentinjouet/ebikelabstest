const fs = require('fs');

const db_file = 'bikesdatabase.json';

function readBikes () {
    // Parse and return bike list from JSON file
    let bikesdata = fs.readFileSync(db_file);
    return JSON.parse(bikesdata);
}

function writeBikes (bikes) {
    // Overwrite bikes JSON file with new bikes list
    fs.writeFileSync(db_file, JSON.stringify(bikes, null, 2), function(err) {
        if (err) {
            // Handle write error
            return console.log(err);
        }
        console.log("Saved !");
    }); 
}

module.exports = { readBikes, writeBikes }