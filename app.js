const express = require('express');
const Joi = require('joi');
const uuidv4 = require('uuid/v4');
const db = require('./dbutils.js');
const app = express();
app.use(express.json());

function validateBike (bike) {
    const schema = {
        name: Joi.string().min(1).required(),
        brand: Joi.string(),
        year: Joi.number()
            .integer()
            .min(1900)
            .max(new Date().getFullYear() + 1),
        motorType: Joi.string(),
        type: Joi.string()
    };
    return Joi.validate(bike, schema);
}

// Bikes API endpoint

app.get('/', (req, res) => {
    res.send('ok');
});

app.get('/bikes', (req, res) => {
    res.send(db.readBikes());
});

app.get('/bikes/:id', (req, res) => {
    const bikes = db.readBikes();
    const bike = bikes.find(bike => bike.id === req.params.id);
    if (!bike) return res.status(404).send('Bike not found !');
    res.send(bike);
});

app.post('/bikes', (req, res) => {
    // Validate input
    const { error } = validateBike(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    // Create new bike object
    const bike = {
        id: uuidv4(), // Gen uuid4 id
        ...req.body
    };
    // Append new bike to existing list
    let bikes = db.readBikes();
    bikes.push(bike);
    // Save updated list
    db.writeBikes(bikes);
    // Return new bike
    res.send(bike);
});

app.put('/bikes/:id', (req, res) => {
    // Get bike
    let bikes = db.readBikes();
    const bikeIndex = bikes.findIndex(bike => bike.id === req.params.id);
    let bike = bikes[bikeIndex];
    if (!bike) return res.status(404).send('Bike not found !');
    // Validate input
    const { error } = validateBike(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    // Update
    bike = {
        id: bike.id,
        ...req.body
    };
    bikes[bikeIndex] = bike;
    // Save updated list
    db.writeBikes(bikes);
    // Return updated bike
    res.send(bike);
});

app.delete('/bikes/:id', (req, res) => {
    // Get bike
    let bikes = db.readBikes();
    const bike = bikes.find(bike => bike.id === req.params.id);
    if (!bike) return res.status(404).send('Bike not found !');
    // Remove it from list
    const index = bikes.indexOf(bike);
    bikes.splice(index, 1);
    // Save updated list
    db.writeBikes(bikes);
    // Return deleted bike
    res.send(bike);
});

app.listen(3000, () => console.log('listening on port 3000...'));